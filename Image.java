package assign11;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import javax.imageio.ImageIO;

/**
 * Creates an Image object to be used in Assignment 10 for CS 1410.
 * 
 * @author Omar Rizwan.
 * @version 4/12/24
 */
public class Image {
	private Pixel[][] pixels;
	private Pixel[][] originalPixels;

	/**
	 * Constructor for the Image object: creates an array of Pixels and
	 * OriginalPixels from a given file by iterating throw the pixels assigning the
	 * 2D array of orginalPixels and Pixels to these values.
	 * 
	 * @param filename : the name of a file to be turned into an array of pixels
	 * @throws Exception : FileNotFoundException
	 */
	public Image(String filename) throws Exception {
		BufferedImage image = ImageIO.read(new File(filename));
		pixels = new Pixel[image.getHeight()][image.getWidth()];
		originalPixels = new Pixel[image.getHeight()][image.getWidth()];
		for (int y = 0; y < image.getHeight(); y++) {
			for (int x = 0; x < image.getWidth(); x++) {
				int color = image.getRGB(x, y);
				Pixel pixel = new Pixel((color >> 16) & 0xFF, (color >> 8) & 0xFF, color & 0xFF);
				pixels[y][x] = pixel;
				originalPixels[y][x] = new Pixel(pixel.getRed(), pixel.getGreen(), pixel.getBlue());
			}
		}
	}
	
	/**
	 * Cretes a copy of the Pixels every time a anything is implied
	 * @return copy - Image that is the copy of every manipulated Image.
	 */

	public Pixel[][] CopyPixelsUndo() {
		int rows = getNumberOfRows();
		int cols = getNumberOfColumns();
		Pixel[][] copy = new Pixel[rows][cols];
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				Pixel p = pixels[i][j];
				copy[i][j] = new Pixel(p.getRed(), p.getGreen(), p.getBlue());
			}
		}
		return copy;
	}

	/**
	 * Image constructor for undo used in the try case for apply filters.
	 * @param other
	 */
	public Image(Image other) {
		int rows = other.getNumberOfRows();
		int cols = other.getNumberOfColumns();
		this.pixels = new Pixel[rows][cols];
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				Pixel originalPixel = other.getPixel(i, j);
				this.pixels[i][j] = new Pixel(originalPixel.getRed(), originalPixel.getGreen(),
						originalPixel.getBlue());
			}
		}
	}

	/**
	 * Makes a buffered image to use in the ImagePanel class
	 * 
	 * @return : a buffered image
	 */
	public BufferedImage toBufferedImage() {
		BufferedImage bi = new BufferedImage(getNumberOfColumns(), getNumberOfRows(), BufferedImage.TYPE_INT_RGB);
		for (int y = 0; y < getNumberOfRows(); y++) {
			for (int x = 0; x < getNumberOfColumns(); x++) {
				Pixel p = getPixel(y, x);
				bi.setRGB(x, y, p.getPackedRGB());
			}
		}
		return bi;
	}

	/**
	 * Swaps the red and blue values of an image
	 */
	public void redBlueSwapFilter() {
		for (int i = 0; i < getNumberOfRows(); i++) {
			for (int j = 0; j < getNumberOfColumns(); j++) {
				Pixel p = getPixel(i, j);
				int red = p.getRed();
				int blue = p.getBlue();
				pixels[i][j] = new Pixel(blue, p.getGreen(), red);
			}
		}
	}

	/**
	 * Makes an image black and white
	 */
	public void grayscaleFilter() {
		int rows = getNumberOfRows();
		int cols = getNumberOfColumns();
		Pixel[][] newPixels = new Pixel[rows][cols];

		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				Pixel p = getPixel(i, j);

				int gray = (int) (0.299 * p.getRed() + 0.587 * p.getGreen() + 0.114 * p.getBlue());
				newPixels[i][j] = new Pixel(gray, gray, gray);
			}
		}
		pixels = newPixels;
	}

	/**
	 * Rotates an image clockwise
	 */
	public void rotateClockwiseFilter() {
		Pixel[][] newPixels = new Pixel[getNumberOfColumns()][getNumberOfRows()];
		for (int i = 0; i < getNumberOfRows(); i++) {
			for (int j = 0; j < getNumberOfColumns(); j++) {
				newPixels[j][getNumberOfRows() - 1 - i] = getPixel(i, j);
			}
		}
		pixels = newPixels;
	}

	/**
	 * Inverts the colors of an image Made by Omar Rizwan
	 */
	public void negativeFilter() {
		int rows = getNumberOfRows();
		int cols = getNumberOfColumns();
		Pixel[][] newPixels = new Pixel[rows][cols];
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				Pixel p = getPixel(i, j);
				int negativeRed = 255 - p.getRed();
				int negativGreen = 255 - p.getGreen();
				int negativBlue = 255 - p.getBlue();
				newPixels[i][j] = new Pixel(negativeRed, negativGreen, negativBlue);
			}
		}
		pixels = newPixels;
	}

	/**
	 * Rotates an image counterclockwise
	 */

	public void rotateCounterClockwiseFilter() {
		Pixel[][] newPixels = new Pixel[getNumberOfColumns()][getNumberOfRows()];
		for (int i = 0; i < getNumberOfRows(); i++) {
			for (int j = 0; j < getNumberOfColumns(); j++) {

				newPixels[getNumberOfColumns() - 1 - j][i] = getPixel(i, j);
			}
		}
		pixels = newPixels;
	}

	/**
	 * Takes the greatest of the RGB (e.g. for (10, 50, 100) you take 100) and set
	 * all other colors to 0. If 2 colors are greater than one and equal to each
	 * other then they both stay and the lesser becomes 0. If all 3 are the same
	 * then the color stays the same. The filter also mirrors the image.
	 * 
	 */
	public void mirrorMaximize() {
		int rows = pixels.length;
		int cols = pixels[0].length;
		Pixel[][] moddedArray = new Pixel[rows][cols];

		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				Pixel currentPixel = pixels[i][j];
				int red = currentPixel.getRed();
				int green = currentPixel.getGreen();
				int blue = currentPixel.getBlue();

				if (red == green && green == blue) {

					moddedArray[i][cols - 1 - j] = new Pixel(red, green, blue);
				} else {
					int max = Math.max(Math.max(red, green), blue);
					int newRed = (red == max) ? red : 0;
					int newGreen = (green == max) ? green : 0;
					int newBlue = (blue == max) ? blue : 0;

					moddedArray[i][cols - 1 - j] = new Pixel(newRed, newGreen, newBlue);
				}
			}
		}

		pixels = moddedArray;
	}

	/**
	 * Resets an image to where it was before a filter was applied
	 */
	public void resetImage() {

		pixels = originalPixels;
	}

	/**
	 * Getter for the number of rows
	 * 
	 * @return : the number of rows
	 */
	public int getNumberOfRows() {
		return pixels.length;
	}

	/**
	 * Getter for the number of columns
	 * 
	 * @return : the number of columns
	 */

	public int getNumberOfColumns() {
		if (pixels.length == 0)
			return 0;
		return pixels[0].length;
	}

	/**
	 * Sets the brightness of the Image to negative or Positive as slider X is
	 * adjusted.
	 * 
	 * @param brightnessLevel - the level of the brightness the pixels are needed to
	 *                        set
	 */
	public void Brightness(int brightnessLevel) {
		int rows = pixels.length;
		int cols = pixels[0].length;
		Pixel[][] moddedArrayBrightness = new Pixel[rows][cols];

		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				Pixel currentPixel = pixels[i][j];
				int red = currentPixel.getRed();
				int green = currentPixel.getGreen();
				int blue = currentPixel.getBlue();

				int newRed = red + brightnessLevel;
				int newGreen = green + brightnessLevel;
				int newBlue = blue + brightnessLevel;

				if (newRed > 255) {
					newRed = 255;
				} else if (newRed < 0) {
					newRed = 0;
				}

				if (newGreen > 255) {
					newGreen = 255;
				} else if (newGreen < 0) {
					newGreen = 0;
				}

				if (newBlue > 255) {
					newBlue = 255;
				} else if (newBlue < 0) {
					newBlue = 0;
				}

				moddedArrayBrightness[i][j] = new Pixel(newRed, newGreen, newBlue);
			}
		}

		pixels = moddedArrayBrightness;
	}

	
	
	/**
	 * Changes the red value of the Image based off of the Red slider
	 * @param initialRed : the starting value for the red slider
	 */
	public void red(int initialRed) {
		int rows = pixels.length;
		int cols = pixels[0].length;
		Pixel[][] moddedRedArray = new Pixel[rows][cols];

		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				Pixel currentPixel = pixels[i][j];
				int red = currentPixel.getRed();
				int green = currentPixel.getGreen();
				int blue = currentPixel.getBlue();

				int newRed = red + initialRed;

				if (newRed > 255) {
					newRed = 255;
				} else if (newRed < 0) {
					newRed = 0;
				}

				moddedRedArray[i][j] = new Pixel(newRed, green, blue);
			}
		}

		pixels = moddedRedArray;
	}
	/**
	 * Changes the green value of the Image based off of the Green slider
	 * @param initialGreen : the starting value for the green slider
	 */
	public void green(int initialGreen) {
		int rows = pixels.length;
		int cols = pixels[0].length;
		Pixel[][] moddedGreenArray = new Pixel[rows][cols];

		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				Pixel currentPixel = pixels[i][j];
				int red = currentPixel.getRed();
				int green = currentPixel.getGreen();
				int blue = currentPixel.getBlue();

				int newGreen = green + initialGreen;

				if (newGreen > 255) {
					newGreen = 255;
				} else if (newGreen < 0) {
					newGreen = 0;
				}

				moddedGreenArray[i][j] = new Pixel(red, newGreen, blue);
			}
		}

		pixels = moddedGreenArray;
	}
	/**
	 * Changes the blue value of the Image based off of the Blue slider
	 * @param initialBlue : the starting value for the blue slider
	 */
	public void blue(int initialBlue) {
		int rows = pixels.length;
		int cols = pixels[0].length;
		Pixel[][] moddedBlueArray = new Pixel[rows][cols];

		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				Pixel currentPixel = pixels[i][j];
				int red = currentPixel.getRed();
				int green = currentPixel.getGreen();
				int blue = currentPixel.getBlue();

				int newBlue = blue + initialBlue;

				if (newBlue > 255) {
					newBlue = 255;
				} else if (newBlue < 0) {
					newBlue = 0;
				}

				moddedBlueArray[i][j] = new Pixel(red, green, newBlue);
			}
		}

		pixels = moddedBlueArray;
	}

	/**
	 * Getter for a specific pixel
	 * 
	 * @param row : row of the pixel
	 * @param col : column of the pixel
	 * @return : a specific pixel of an image
	 */
	public Pixel getPixel(int row, int col) {
		return pixels[row][col];
	}
/**
 * 
 * @param x1 - X coordinate of the starting point of mouse clicked.
 * @param y1 - Y coordinate of the starting point of mouse clicked.
 * @param x2 - X coordinate of the Ending point of mouse clicked.
 * @param y2 - Y coordinate of the Ending point of mouse clicked.
 */
	public void crop(int x1, int y1, int x2, int y2) {
		int startX = x1;
		int endX = x2;
		if (x1 > x2) {
			startX = x2;
			endX = x1;
		}

		int startY = y1;
		int endY = y2;
		if (y1 > y2) {
			startY = y2;
			endY = y1;
		}
		int width = x2 - x1;
		if(width < 0)
			width = -(width);

		int height = y2 - y1;
		if(height < 0)
			height = -(height);

		Pixel[][] croppedPixels = new Pixel[height][width];

		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				croppedPixels[y][x] = pixels[startY + y][startX + x];
			}
		}

		pixels = croppedPixels;
	}

}
