package assign11;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

/**
 * This class represents a GUI component for displaying an image.
 *
 * @version Assignment 10
 */
public class ImagePanel extends JPanel implements MouseListener, MouseMotionListener {
	private static final long serialVersionUID = 1L;
	private Image image;
	private BufferedImage bufferedImg;
	private int startX, startY, endX, endY;
	public static boolean isCropping = false;

	public static void ChangeisCropping() {
		isCropping = false;
	}

	/**
	 * Constructor for the ImagePanel class.
	 * 
	 * @param img the image to put in the imagePanel
	 */
	public ImagePanel(Image img) {
		this.image = img;
		addMouseListener(this);
		addMouseMotionListener(this);
		updateBufferedImage();
	}

	/**
	 * Setter for the ImagePanel class.
	 * 
	 * @param img the image to put in the imagePanel
	 */
	public void setImage(Image img) {
		this.image = img;
		updateBufferedImage();
	}

	/**
	 * Getter for the image in the ImagePanel.
	 * 
	 * @return the image in the ImagePanel
	 */
	public Image getImage() {
		return image;
	}

	/**
	 * Getter for the buffered image in the ImagePanel.
	 * 
	 * @return the BufferedImage for display and saving
	 */
	public BufferedImage getBufferedImage() {
		return bufferedImg;
	}

	/**
	 * Updates the buffered image.
	 */
	private void updateBufferedImage() {
		if (image != null) {
			int rowCount = image.getNumberOfRows();
			int colCount = image.getNumberOfColumns();
			bufferedImg = new BufferedImage(colCount, rowCount, BufferedImage.TYPE_INT_RGB);
			for (int y = 0; y < rowCount; y++) {
				for (int x = 0; x < colCount; x++) {
					bufferedImg.setRGB(x, y, image.getPixel(y, x).getPackedRGB());
				}
			}
			repaint();
		}
	}

	/**
	 * Repaints the image in the ImagePanel.
	 */
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (bufferedImg != null) {
			g.drawImage(bufferedImg, 0, 0, getWidth(), getHeight(), this);
			if (ImageProcessorFrame.getCroppingToggle() == true) {
				if (isCropping) {
					g.setColor(new Color(105, 105, 105, 125));
					int x = Math.min(startX, endX);
					int y = Math.min(startY, endY);
					int width = Math.abs(endX - startX);
					int height = Math.abs(endY - startY);
					g.fillRect(x, y, width, height);
				}
			}

		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		if (ImageProcessorFrame.getCroppingToggle()) {
			if (isCropping = true) {
				startX = e.getX();
				startY = e.getY();
			}
		}
	}

	public void mouseReleased(MouseEvent e) {
		if (ImageProcessorFrame.getCroppingToggle()) {
			if (isCropping = true) {
				int cropStartPointX;
				int cropStartPointY;
				int cropEndPointX;
				int cropEndPointY;
				if (startX < e.getX()) {
					cropStartPointX = (int) (startX);
					cropEndPointX = (int) (e.getX());
				} else {
					cropStartPointX = (int) (e.getX());
					cropEndPointX = (int) (startX);
				}
				if (startY < e.getY()) {
					cropStartPointY = (int) (startY);
					cropEndPointY = (int) (e.getY());
				} else {
					cropStartPointY = (int) (e.getY());
					cropEndPointY = (int) (startY);
				}
				image.crop(cropStartPointX, cropStartPointY, cropEndPointX, cropEndPointY);
				updateBufferedImage();
				// Reset the Points
				startX = 0;
				startY = 0;
				endX = 0;
				endY = 0;
			}
		}
		isCropping = false;
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		if (isCropping) {
			endX = e.getX();
			endY = e.getY();
			repaint();
		}
	}

	@Override
	public void mouseMoved(MouseEvent e) {
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	/**
	 * Enables or disables cropping mode.
	 * 
	 * @param mode true to enable cropping, false to disable
	 */
	public void setCropMode(boolean mode) {
		isCropping = mode;
	}
}
