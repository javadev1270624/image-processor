package assign11;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.PaintContext;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * 
 * Holds all the GUI implementation for the code of Assignment 11 Part 1 (CS
 * 1410) Creates a Graphical User Interface which allows you to import an image from your personal
 * device and manipulate it using several features . Users can then save the
 * file they manipulated or reset it as well.
 * 
 * @author Omar Rizwan.
 * @version 4/12/24
 */
public class ImageProcessorFrame extends JFrame implements ActionListener, ChangeListener {
	private static final long serialVersionUID = 1L;
	private ImagePanel imagePanel;
	private JMenuItem openItem, saveItem;
	private JFileChooser fileChooser;
	private JMenu filterMenu;
	private JSlider sliderX;
	private JButton Close = new JButton("Close");
	private JPanel controlPanel = new JPanel(new BorderLayout());
	public ArrayList<Image> imageArray = new ArrayList<>();
	JPanel brightnessControlPanel = new JPanel();
	public static boolean Croppingtoggle = false;
	JPanel CroppingPanel = new JPanel();
	private JButton CloseCrop = new JButton("CloseCrop");
	private JPanel customControlPanel = new JPanel();
	private JButton Apply = new JButton("Apply");
	private JSlider sliderR, sliderG, sliderB;

	/**
	 * Constructor for the ImageProcessorFrame object Creates GUI, a JFrame and
	 * implements the JPanel to access files and filter images
	 */
	public ImageProcessorFrame() {
		super("Image Processor");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(1000, 800);
		setLocationRelativeTo(null);
		setLayout(new BorderLayout());

		fileChooser = new JFileChooser();
		fileChooser.setFileFilter(new FileNameExtensionFilter("Image files", "jpg", "jpeg", "png", "bmp"));

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		JMenu fileMenu = new JMenu("File");
		filterMenu = new JMenu("Filters");
		menuBar.add(fileMenu);
		menuBar.add(filterMenu);

		openItem = new JMenuItem("Open new image file...");
		openItem.addActionListener(this);
		fileMenu.add(openItem);

		saveItem = new JMenuItem("Save filtered image file...");
		saveItem.addActionListener(this);
		saveItem.setEnabled(false);
		fileMenu.add(saveItem);

		setupFilters(filterMenu);
		sliderX = new JSlider(-200, 255, 50);
		sliderX.setMinorTickSpacing(20);
		sliderX.setMajorTickSpacing(30);
		sliderX.setPaintTicks(true);
		sliderX.setPaintLabels(true);
		sliderX.addChangeListener(this);

		brightnessControlPanel.setLayout(new BorderLayout());
		JPanel buttonsPanel = new JPanel();
		buttonsPanel.add(Close);
		Close.addActionListener(this);

		brightnessControlPanel.add(buttonsPanel, BorderLayout.SOUTH);
		brightnessControlPanel.add(sliderX, BorderLayout.CENTER);
		brightnessControlPanel.setVisible(false);
		controlPanel.add(brightnessControlPanel, BorderLayout.SOUTH);
		add(controlPanel, BorderLayout.SOUTH);

		CroppingPanel.add(CloseCrop, BorderLayout.CENTER);
		CroppingPanel.setVisible(false);
		add(CroppingPanel, BorderLayout.NORTH);
		CloseCrop.addActionListener(this);
		JMenuItem undoMenuItem = new JMenuItem("Undo");
		undoMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Undo();
			}
		});
		filterMenu.add(undoMenuItem);

		sliderR = new JSlider(SwingConstants.VERTICAL, -200, 255, 50);
		sliderR.setMinorTickSpacing(20);
		sliderR.setMajorTickSpacing(30);
		sliderR.setPaintTicks(true);
		sliderR.setPaintLabels(true);
		sliderR.addChangeListener(this);

		sliderG = new JSlider(SwingConstants.VERTICAL, -200, 255, 50);
		sliderG.setMinorTickSpacing(20);
		sliderG.setMajorTickSpacing(30);
		sliderG.setPaintTicks(true);
		sliderG.setPaintLabels(true);
		sliderG.addChangeListener(this);

		sliderB = new JSlider(SwingConstants.VERTICAL, -200, 255, 50);
		sliderB.setMinorTickSpacing(20);
		sliderB.setMajorTickSpacing(30);
		sliderB.setPaintTicks(true);
		sliderB.setPaintLabels(true);
		sliderB.addChangeListener(this);

		customControlPanel.setLayout(new BorderLayout());
		JPanel applyPanel = new JPanel();
		applyPanel.add(Apply);
		Apply.addActionListener(this);
		JLabel manipulateLabel = new JLabel("Change the RGB values");

		customControlPanel.add(applyPanel, BorderLayout.SOUTH);
		customControlPanel.add(manipulateLabel, BorderLayout.NORTH);
		customControlPanel.add(sliderR, BorderLayout.WEST);
		customControlPanel.add(sliderG, BorderLayout.CENTER);
		customControlPanel.add(sliderB, BorderLayout.EAST);
		customControlPanel.setVisible(false);
		add(customControlPanel, BorderLayout.EAST);

	}

	/**
	 * Sets up the filters for the JFIleMenu
	 * 
	 * @param filterMenu : takes the filter JMenu object and applies the JMenuItems
	 *                   to it
	 */
	private void setupFilters(JMenu filterMenu) {
		String[] filters = { "Red-Blue Swap", "Black and White", "Rotate Clockwise", "Negative Filter",
				"Rotate Counter-Clockwise", "Mirror Maximize", "Brightness", "Reset", "Crop", "Manipulate" };
		String[] tooltips = { "Swaps the red and blue color channels in the image.",
				"Converts the image to a grayscale black and white version.", "Rotates the image 90 degrees clockwise.",
				"Inverts all the colors of the image to their opposites.",
				"Rotates the image 90 degrees counter-clockwise.",
				"Removes all but the highest val in the RGB and mirrors the image",
				"Please slide the bar on the bottom to adjust the brightness",
				"Restores the original image, removing all applied filters.", "Allows the User to crop the Image.",
				"TRY YOUR LUCK!" };

		for (int i = 0; i < filters.length; i++) {
			JMenuItem menuItem = new JMenuItem(filters[i]);
			menuItem.setToolTipText(tooltips[i]);
			menuItem.addActionListener(this);
			menuItem.setEnabled(false);
			filterMenu.add(menuItem);
		}
	}

	/**
	 * Takes the JMenu clicks and makes them do things
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JMenuItem) {
			JMenuItem source = (JMenuItem) e.getSource();
			if (source == openItem) {
				openImage();
			} else if (source == saveItem) {
				saveImage();
			} else
				applyFilter(source.getText());
		} else if (e.getSource() instanceof JButton) {
			JButton src = (JButton) e.getSource();
			if (src == Close) {
				Image img = imagePanel.getImage();
				imageArray.add(img);
				brightnessControlPanel.setVisible(false);
			}
			if (src == CloseCrop)
				enableFilters(true);
			CroppingPanel.setVisible(false);
			ImagePanel.ChangeisCropping();
			if (src == Apply) {
				customControlPanel.setVisible(false);
			}
		}
	}

	/**
	 * This allows the brightness panel to appear when the brightness button is
	 * clicked
	 */

	/**
	 * Accesses the files of a computer and opens it to the JMenuItem
	 */
	private void openImage() {
		int returnVal = fileChooser.showOpenDialog(this);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File file = fileChooser.getSelectedFile();
			try {
				Image img = new Image(file.getAbsolutePath());
				imagePanel = new ImagePanel(img);
				JScrollPane scrollPane = new JScrollPane(imagePanel);
				getContentPane().add(scrollPane, BorderLayout.CENTER);
				saveItem.setEnabled(true);
				enableFilters(true);
				validate();

			} catch (Exception ex) {
				JOptionPane.showMessageDialog(this, "Error loading image: " + ex.getMessage(), "Error",
						JOptionPane.ERROR_MESSAGE);
				enableFilters(false);
			}
		}
	}

	/**
	 * Saves the filtered image to a location of the user's choice
	 */
	private void saveImage() {
		if (imagePanel == null || imagePanel.getBufferedImage() == null) {
			JOptionPane.showMessageDialog(this, "No image to save", "Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		fileChooser.setDialogTitle("Save Image");
		fileChooser.setFileFilter(new FileNameExtensionFilter("JPEG file", "jpg"));
		int userChoice = fileChooser.showSaveDialog(this);
		if (userChoice == JFileChooser.APPROVE_OPTION) {
			File fileToSave = fileChooser.getSelectedFile();
			if (!fileToSave.getAbsolutePath().toLowerCase().endsWith(".jpg")) {
				fileToSave = new File(fileToSave.getAbsolutePath() + ".jpg");
			}
			try {
				BufferedImage img = imagePanel.getBufferedImage();
				ImageIO.write(img, "jpg", fileToSave);
				JOptionPane.showMessageDialog(this, "Image saved successfully to " + fileToSave.getAbsolutePath(),
						"Success", JOptionPane.INFORMATION_MESSAGE);
			} catch (IOException ex) {
				JOptionPane.showMessageDialog(this, "Failed to save the image: " + ex.getMessage(), "Error",
						JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	/**
	 * Applies the filters in the JMenu to the image
	 * 
	 * @param filterName : the filter to be applied
	 */
	private void applyFilter(String filterName) {
		if (imagePanel == null || imagePanel.getImage() == null) {
			JOptionPane.showMessageDialog(this, "No image loaded to apply filters", "Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		try {
			Image img = imagePanel.getImage();
			imageArray.add(new Image(imagePanel.getImage()));
			switch (filterName) {
			case "Red-Blue Swap":

				img.redBlueSwapFilter();
				break;
			case "Black and White":

				img.grayscaleFilter();
				break;
			case "Rotate Clockwise":

				img.rotateClockwiseFilter();
				break;
			case "Negative Filter":

				img.negativeFilter();
				break;
			case "Rotate Counter-Clockwise":

				img.rotateCounterClockwiseFilter();
				break;
			case "Reset":

				img.resetImage();
				break;
			case "Mirror Maximize":
				img.mirrorMaximize();
				break;
			case "Brightness":
				img.Brightness(0);
				brightnessControlPanel.setVisible(true);
				break;
			case "Crop":
				Croppingtoggle = true;
				imagePanel.setCropMode(true);
				enableFilters(false);
				CroppingPanel.setVisible(true);
				break;
			case "Manipulate":
				img.red(0);
				img.green(0);
				img.blue(0);
				img.crop(100, 150, 800, 600);
				customControlPanel.setVisible(true);
				break;

			}
			imagePanel.setImage(img);
			imagePanel.repaint();
			saveItem.setEnabled(true);
		} catch (Exception ex) {
			JOptionPane.showMessageDialog(this, "Error applying filter: " + ex.getMessage(), "Error",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * Enables the use of filters on the image
	 * 
	 * @param enable : changes false to true
	 */
	private void enableFilters(boolean enable) {
		for (int i = 0; i < filterMenu.getItemCount(); i++) {
			filterMenu.getItem(i).setEnabled(enable);
		}
	}

	/**
	 * Undo method returns the previous displayed image by remove the last element
	 * for the Arraylist of images
	 */
	public void Undo() {
		if (imageArray.size() > 0) {
			Image lastSavedState = imageArray.remove(imageArray.size() - 1);
			imagePanel.setImage(lastSavedState);
			imagePanel.repaint();
		}
	}

	/**
	 * StateChanged is used for the J slider to Adjust the brightness values. The
	 * Image is reset every time the source of slider X is called so that that the
	 * Image does not use the already brightened Image Pixel value to pass into the
	 * brightness method.
	 * 
	 * @param ChangeEvent e - The Slider of the bar changing the values by dragging
	 *                    it.
	 */
	@Override
	public void stateChanged(ChangeEvent e) {
		JSlider src = (JSlider) e.getSource();
		Image img = imagePanel.getImage();
		if (src == sliderX) {
			img.resetImage();
			int brightnessLevel = src.getValue();
			img.Brightness(brightnessLevel);
			imagePanel.setImage(img);
			imagePanel.repaint();
		} else if (src == sliderR) {
			img.resetImage();

			int redLevel = src.getValue();
			img.red(redLevel);
			imagePanel.setImage(img);
			customControlPanel.setVisible(true);
			imagePanel.repaint();
		} else if (src == sliderG) {

			img.resetImage();
			int greenLevel = src.getValue();
			img.green(greenLevel);
			imagePanel.setImage(img);
			imagePanel.repaint();
			customControlPanel.setVisible(true);
		} else if (src == sliderB) {

			img.resetImage();
			int blueLevel = src.getValue();
			img.blue(blueLevel);
			imagePanel.setImage(img);
			imagePanel.repaint();
			customControlPanel.setVisible(true);
		}
	}

	/**
	 * USed to keep track of the crop function when it is implemented in the
	 * ApplyFilter.
	 * 
	 * @return
	 */
	public static boolean getCroppingToggle() {
		return Croppingtoggle;
	}

}
