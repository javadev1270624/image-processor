package assign11;

import javax.swing.SwingUtilities;

/**
 * Runs the code for the Assignment 10 (part 1) in CS 1410. Copied from the
 * canvas page
 * 
 * @author Omar Rizwan.
 * @version 4/12/24
 */
public class ImageProcessorProgram {

	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> {
			ImageProcessorFrame frame = new ImageProcessorFrame();
			frame.setVisible(true);
		});
	}
}
