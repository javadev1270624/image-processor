package assign11;

/**
 * This class defines the RGB vals for a pixel in an image. Created for
 * assignment 10 in CS 1410.
 * 
 * @author Omar Rizwan.
 * @version 3/4/24
 */
public class Pixel {
	private int red;
	private int green;
	private int blue;

	/**
	 * setter for the RGB vals of the Pixel object
	 * 
	 * @param red   : the red of the RGB
	 * @param green : the green of the RGB
	 * @param blue  : the blue of the RGB
	 */
	public Pixel(int red, int green, int blue) {
		this.red = red;
		this.green = green;
		this.blue = blue;
	}

	/**
	 * getter for the R val of the RGB
	 * 
	 * @return : the R val of the RGB
	 */
	public int getRed() {
		return red;
	}

	/**
	 * getter for the G val of the RGB
	 * 
	 * @return : the G val of the RGB
	 */
	public int getGreen() {
		return green;
	}

	/**
	 * getter for the B val of the RGB
	 * 
	 * @return : the B val of the RGB
	 */
	public int getBlue() {
		return blue;
	}

	/**
	 * makes the packed value for an RGB
	 * 
	 * @return : the packed value for the RGB
	 */
	public int getPackedRGB() {
		return (red << 16) | (green << 8) | blue;
	}
}
