# Image Processor

The Java Swing Image Processor is an advanced desktop application that empowers users to import images from their computers and perform a wide range of pixel-level manipulations and edits. Leveraging the robust Java Swing framework, this program provides a powerful and intuitive platform for image processing.

With this application, users can seamlessly import images in various formats and utilize a comprehensive suite of tools to manipulate pixels, apply changes, and crop images to their desired specifications. The Java Swing framework ensures a responsive and user-friendly interface, facilitating an efficient and enjoyable editing experience. This image processor is designed to cater to both novice and experienced users, offering a versatile solution for all image editing needs.

## Tech

 - Java
    - Swing
    - ImageIO
    - Util
    - JUnit
    
## Setup
 1. `git clone https://gitlab.com/javadev1270624/image-processor.git`
 2. `cd image-processor`
 

## Run
 1. `javac ImageProcessorProgram.java`
 2. `java ImageProcessorProgram.java`


## Img
![Image Processor](https://gitlab.com/javadev1270624/image-processor/-/raw/main/image-processor.png)

